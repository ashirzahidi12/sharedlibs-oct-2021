# stage 1
#FROM node:14.15.2-alpine AS builder
#RUN mkdir -p /app
#WORKDIR /app
#COPY package.json /app
#RUN npm install 
#COPY . /app
#RUN npm run build --prod

# stage 2
#FROM nginx:alpine
#COPY --from=builder /app/dist/rc-teaf /usr/share/nginx/html
#EXPOSE 80



# stage 1


FROM node:14.15.2-alpine AS builder
WORKDIR /app
COPY . .
RUN npm install
RUN npm install -g @angular/cli@11.0.5
RUN ng build --configuration=dev
#RUN npm install && npm run build --configuration=dev --base-href=/rc-teaf/



# stage 2


FROM nginx:alpine
COPY --from=builder /app/dist/rc-teaf /usr/share/nginx/html
EXPOSE 80
EXPOSE 4300



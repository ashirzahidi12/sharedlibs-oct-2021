pipeline {
    agent { label 'TEAF' }

    stages {
         stage ('Docker Image') {
            steps {
                sh 'docker build -t royalcyber/teaf-client:$BUILD_NUMBER .'
            }
        }
        
    stage ('Docker Push') {
            steps {
            withCredentials([string(credentialsId: 'DockerHub_Pwd', variable: 'DockerHub_Pwd')]) {
                sh "docker login -u royalcyber -p ${DockerHub_Pwd} "
    
            }            
                sh 'docker push royalcyber/teaf-client:$BUILD_NUMBER'
            }
        }
        
    stage ('Docker Deploy') {
            steps {                       
                sh 'docker stop teaf-client'
                sh 'docker rm teaf-client'
                sh 'docker run -d --publish 80:80 --name teaf-client royalcyber/teaf-client:$BUILD_NUMBER'                
            }
        }


        
    }

}